# Rails - API - chat - client

Minimalist client chat application developed to test the [Rails - API - chat][rails_api_chat] server application.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- GIT
- Rails
- [Rails - API - chat][rails_api_chat] server application

### Installation

```
$ git clone https://Yohan_@bitbucket.org/Yohan_/rails_api_chat_client.git
```
```
$ cd rails_api_chat_client
```
```
$ bundle install
```

### Optionnal configuration

If you made some changes on your [Rails - API - chat][rails_api_chat] server URL, configure the correct URL:

1. for the API

		$ vi config/service_api.yml

	Update the line:

		url: http://localhost:3003/v1/


2. for Action Cable

		$ vi config/environments/development.rb

	Update the line:

		# Action Cable uri
		config.action_cable.url = 'ws://localhost:3003/cable'

## Usage

1. You need to have the [Rails - API - chat][rails_api_chat] server running.
2. In your project directory, start the server for the client application:

		$ rails server

3. First user
	1. Reach the home page [http://localhost:3000/][localhost] in your browser.
	2. Create a user.
	3. Create a room.
4. Second user
	1. Open a new browser window using the privacy mode or an other browser and follow the step 3.1 and 3.2.
	2. Join the room of the first user.
5. Chat!

## Known Issues

- There is a unique Action Cable chat channel, so the messages will belong to a unique room but will be broadcasted to all rooms when sent.
- The application don't handle the error response when the API server url is wrong.

[rails_api_chat]: https://bitbucket.org/Yohan_/rails_api_chat
[localhost]: http://localhost:3000/