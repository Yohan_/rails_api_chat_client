class ApiService < ApplicationService

	attr_reader :url_api

	def initialize(resource_name, token = nil)
		@url_api = Rails.configuration.service_api['url']
		@resource_name = resource_name
		@headers = {
			'Content-Type'  => 'application/json',
			'Accept'  => 'application/json'
			}
		if token.present?
			@headers['Authorization'] = 'Token token="' + token + '"'
		end
	end

	def get(resource_id = nil)
		@resource_path = get_resource_path
		@resource_path += '/' + resource_id if resource_id.present?

		HTTParty.get(@resource_path, :headers => @headers)
	end

	def get_with_filter(filter_name, filter_value)
		@resource_path = get_resource_path + '?' + filter_name + '=' + filter_value

		HTTParty.get(@resource_path, :headers => @headers)
	end

	def post(param)
		HTTParty.post(get_resource_path, 
			:body => param.to_json,
			:headers => @headers)
	end

	private

	def get_resource_path
		@url_api + @resource_name + '/'
	end
end
