$(document).ready( function () {

  // Attach a submit handler to the form
  $('#message-sender').submit(function(event) {

    // Stop form from submitting normally
    event.preventDefault();

    // Get data from the form 
    var $formData = $(this).serializeArray();
    // GET the POST action url
    var $url_api = $(this).attr('action');
    // Get the user token from cookie
    var $token = Cookies.getJSON('user_info').token;

    $.ajax({
      method: "POST",
      url: $url_api,
      data: $formData,
      headers: {
        'Authorization' : 'Token token=' + $token 
      }
    })
    .done(function(msg) {
      // Clean the message box
      $('#message_body').val('');
    });
  });
});