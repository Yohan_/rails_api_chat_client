module ApplicationHelper
	def authentication_info
		html = '<div id="user-info">'
		if current_user
			html += 'User name: '+ current_user['name']
		else
			html += 'User name: guest'
		end
		html += '</div>'
		html.html_safe
	end
end
