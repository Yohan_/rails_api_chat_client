class RoomsController < ApplicationController
  before_action :register_to_api_service

  def index
    @rooms = check_httparty_response(@api.get)
  end

  def show
    @url_api = @api.url_api

    @room = @api.get(params[:id])

    @api_message = register_to_api_service('messages')
    @messages = @api_message.get_with_filter('room_id', params[:id])

    @api_user = register_to_api_service('users')
    @users = check_httparty_response(@api_user.get())
  end

  def create
    result = @api.post(params['room_data'])
    if result['id'].present?
      redirect_to action: 'show', id: result['id']
    else
      if result['name'].present?
        flash[:notice] = 'The name "' + params['room_data']['name'] + '" ' + result['name'][0]
      end
      redirect_to rooms_path
    end
  end

  def destroy
  end
end
