class ApplicationController < ActionController::Base
	protect_from_forgery with: :null_session

	helper_method :current_user

	protected

	def register_to_api_service(resource_name = nil)
		resource_name = controller_name if ! resource_name.present?
		@api = ApiService.new(resource_name, current_user_token)
	end

	def check_httparty_response(httparty_response)
		if httparty_response.code != 200
			flash[:notice] = "Error code: " + httparty_response.code.to_s + " - " + httparty_response['error'].to_s
			# TODO: find another way to handler the error (double redirection issue here)
			redirect_to root_path
		else
			httparty_response
		end
	end

	def current_user
		if cookies[:user_info].present?
			JSON.parse(cookies[:user_info])
		end
	end

	def current_user_token
		if current_user.present?
			current_user['token']
		end
	end
end
