class UsersController < ApplicationController
  before_action :register_to_api_service

  def index
  end

  def create
    result = @api.post(params['user_data'])
    if result['id'].present?
      cookies.delete :user_info
      cookies[:user_info] = result
      flash[:notice] = 'User was successfully created.'
      redirect_to rooms_path
    else
      flash[:notice] = 'The name "' + params['user_data']['name'] + '" ' + result['name'][0]
      redirect_to root_path
    end
  end

  def destroy
  end
end
