Rails.application.routes.draw do

  root "users#index"

  # messages
  post 'messages', to: 'messages#create'

  # rooms
  get 'rooms', to: 'rooms#index'
  get 'rooms/:id', to: 'rooms#show'
  post 'rooms', to: 'rooms#create'
  get 'rooms/destroy'

  # users
  post 'users', to: 'users#create'
  get 'users/destroy'

  # Serve websocket cable requests in-process
  mount ActionCable.server, at: '/cable'

end
